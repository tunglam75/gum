/**
 * --------------------------------------------------------------------------
 * Bootstrap (v4.4.1): scrollspy.js
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * --------------------------------------------------------------------------
 */

import $ from 'jquery';
import Util from 'bootstrap/js/src/util.js';

/**
 * ------------------------------------------------------------------------
 * Constants
 * ------------------------------------------------------------------------
 */

const NAME = 'scrollspy';
const DATA_KEY = 'bs.scrollspy';
const EVENT_KEY = `.${DATA_KEY}`;
const DATA_API_KEY = '.data-api';
const JQUERY_NO_CONFLICT = $.fn[NAME];

const Default = {
  offset: 10,
  method: 'auto',
  target: ''
};

const DefaultType = {
  offset: 'number',
  method: 'string',
  target: '(string|element)'
};

const Event = {
  SCROLL: `scroll${EVENT_KEY}`,
  LOAD_DATA_API: `load${EVENT_KEY}${DATA_API_KEY}`
};

const ACTIVECLASS = 'active';

const Selector = {
  DATA_SPY: '[data-spy="scroll"]',
  ACTIVE: '.active',
  NAV_LINKS: '.g-link'
};

const OffsetMethod = {
  OFFSET: 'offset',
  POSITION: 'position'
};

let skipEl = null;
/**
 * ------------------------------------------------------------------------
 * Class Definition
 * ------------------------------------------------------------------------
 */

class ScrollSpy {
  constructor(element, config) {
    this._element = element;
    this._scrollElement = element.tagName === 'BODY' ? window : element;
    this._config = this._getConfig(config);
    this._selector = `${this._config.target} ${Selector.NAV_LINKS}`;
    this._offsets = [];
    this._targets = [];
    this._activeTarget = null;
    this._scrollHeight = 0;

    $(this._scrollElement).on(Event.SCROLL, event => this._process(event));
    this.refresh();
    this._process();
  }

  static get Default() {
    return Default;
  }

  // Public

  refresh() {
    const autoMethod =
      this._scrollElement === this._scrollElement.window
        ? OffsetMethod.OFFSET
        : OffsetMethod.POSITION;

    const offsetMethod =
      this._config.method === 'auto' ? autoMethod : this._config.method;

    const offsetBase =
      offsetMethod === OffsetMethod.POSITION ? this._getScrollTop() : 0;

    this._offsets = [];
    this._targets = [];

    this._scrollHeight = this._getScrollHeight();

    const targets = [].slice.call(document.querySelectorAll(this._selector));

    targets
      .map(element => {
        let target;
        const targetSelector = Util.getSelectorFromElement(element);
        if (targetSelector) {
          target = document.querySelector(targetSelector);
        }

        if (target) {
          const targetBCR = target.getBoundingClientRect();
          if (targetBCR.width || targetBCR.height) {
            // TODO (fat): remove sketch reliance on jQuery position/offset
            return [$(target)[offsetMethod]().top + offsetBase, targetSelector];
          }
        }
        return null;
      })
      .filter(item => item)
      .sort((a, b) => a[0] - b[0])
      .forEach(item => {
        this._offsets.push(item[0]);
        this._targets.push(item[1]);
      });
  }

  dispose() {
    $.removeData(this._element, DATA_KEY);
    $(this._scrollElement).off(EVENT_KEY);

    this._element = null;
    this._scrollElement = null;
    this._config = null;
    this._selector = null;
    this._offsets = null;
    this._targets = null;
    this._activeTarget = null;
    this._scrollHeight = null;
  }

  // Private

  _getConfig(config) {
    config = {
      ...Default,
      ...(typeof config === 'object' && config ? config : {})
    };
    if (typeof config.target !== 'string') {
      let id = $(config.target).attr('id');
      if (!id) {
        id = Util.getUID(NAME);
        $(config.target).attr('id', id);
      }
      config.target = `#${id}`;
    }

    Util.typeCheckConfig(NAME, config, DefaultType);

    return config;
  }

  _getScrollTop() {
    return this._scrollElement === window
      ? this._scrollElement.pageYOffset
      : this._scrollElement.scrollTop;
  }

  _getScrollHeight() {
    return (
      this._scrollElement.scrollHeight ||
      Math.max(
        document.body.scrollHeight,
        document.documentElement.scrollHeight
      )
    );
  }

  _getOffsetHeight() {
    return this._scrollElement === window
      ? window.innerHeight
      : this._scrollElement.getBoundingClientRect().height;
  }

  _process() {
    const offset = $('header').outerHeight() + 5;
    const scrollTop = this._getScrollTop() + offset;
    const scrollHeight = this._getScrollHeight();
    const maxScroll = offset + scrollHeight - this._getOffsetHeight();

    if (this._scrollHeight !== scrollHeight) {
      this.refresh();
    }

    if (scrollTop >= maxScroll) {
      const target = this._targets[this._targets.length - 1];

      if (this._activeTarget !== target) {
        this._activate(target);
      }
      return;
    }
    if (
      this._activeTarget &&
      scrollTop < this._offsets[0] &&
      this._offsets[0] > 0
    ) {
      this._activeTarget = null;
      this._clear();
      return;
    }
    const offsetLength = this._offsets.length;
    for (let i = offsetLength; i--; ) {
      const isActiveTarget =
        this._activeTarget !== this._targets[i] &&
        scrollTop >= this._offsets[i] &&
        (typeof this._offsets[i + 1] === 'undefined' ||
          scrollTop < this._offsets[i + 1]);

      if (isActiveTarget) {
        this._activate(this._targets[i]);
      }
    }
  }

  _activate(target) {
    if (!skipEl || target === skipEl) {
      this._activeTarget = target;

      this._clear();
      const $link = $(`${this._selector}[href="${target}"]`);
      // Set triggered link as active
      $link.addClass(ACTIVECLASS);
    }
  }

  _clear() {
    [].slice
      .call(document.querySelectorAll(this._selector))
      .filter(node => node.classList.contains(ACTIVECLASS))
      .forEach(node => node.classList.remove(ACTIVECLASS));
  }

  // Static

  static _jQueryInterface(config) {
    return this.each(function() {
      let data = $(this).data(DATA_KEY);
      const _config = typeof config === 'object' && config;

      if (!data) {
        data = new ScrollSpy(this, _config);
        $(this).data(DATA_KEY, data);
      }

      if (typeof config === 'string') {
        if (typeof data[config] === 'undefined') {
          throw new TypeError(`No method named "${config}"`);
        }
        data[config]();
      }
    });
  }
}

/**
 * ------------------------------------------------------------------------
 * Data Api implementation
 * ------------------------------------------------------------------------
 */

$(window).on(Event.LOAD_DATA_API, () => {
  const scrollSpys = [].slice.call(
    document.querySelectorAll(Selector.DATA_SPY)
  );
  const scrollSpysLength = scrollSpys.length;

  for (let i = scrollSpysLength; i--; ) {
    const $spy = $(scrollSpys[i]);
    ScrollSpy._jQueryInterface.call($spy, $spy.data());
  }
  $(document).ready(function() {
    $(document).on('click', 'a[href^="#"]', function(e) {
      e.preventDefault();
      skipEl = $.attr(this, 'href');
      const target = $(skipEl);
      if (target) {
        $('html, body').stop();
        $('html, body').animate(
          {
            scrollTop: target.offset().top - $('header').outerHeight()
          },
          300
        );
      }
    });
  });
});

/**
 * ------------------------------------------------------------------------
 * jQuery
 * ------------------------------------------------------------------------
 */

$.fn[NAME] = ScrollSpy._jQueryInterface;
$.fn[NAME].Constructor = ScrollSpy;
$.fn[NAME].noConflict = () => {
  $.fn[NAME] = JQUERY_NO_CONFLICT;
  return ScrollSpy._jQueryInterface;
};

export default ScrollSpy;
