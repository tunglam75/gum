import Vue from 'vue';
import App from './App.vue';
import './registerServiceWorker';

import './assets/js/scrollspy';
window.$ = window.jQuery = require('jquery');

// custom css
import '../src/assets/scss/index.scss';

// slider - slick
import Slick from 'vue-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
Vue.component('slick', Slick);

// scrollspy
import Scrollspy from 'vue-scrollspy';
Vue.use(Scrollspy);

// import router from './router'

Vue.config.productionTip = false;
new Vue({
  // router,
  render: h => h(App)
}).$mount('#app');
